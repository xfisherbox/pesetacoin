package main

import "testing"


var pesetaCoin = NewClient("http://a:b@localhost:14022/", 6);


// 0.001 (pesetaCoin) min

func TestSetFee(t *testing.T) {

  resp, err := pesetaCoin.SetFee(0.001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := pesetaCoin.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := pesetaCoin.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := pesetaCoin.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := pesetaCoin.GetBalanceByAddress("LGQ79LqpN7Kn22Ktv84kTxFCgCoNw7npL3")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "KwzMsksue7Xx99RKGQCyL4RuHCpVxA55Eo"
  resp, err := pesetaCoin.SendToAddress(addr, 0.6)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := pesetaCoin.GetTransaction("3643d34aec85eb59a177dadeed6ee1e05a2b7d7e260eb03cb9eeee4b5d3b0e49")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := pesetaCoin.CheckTransaction("3643d34aec85eb59a177dadeed6ee1e05a2b7d7e260eb03cb9eeee4b5d3b0e49")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
